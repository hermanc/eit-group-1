export const dialogues = {
  A: {
    question: "Will you send Eva or her younger brother Ragnar to the war?",
    answers: [
      {
        reply: "Send Eva",
        next: "C",
      },
      {
        reply: "Send Ragnar",
        next: "B",
      },
    ],
  },
  B: {
    question: "The enemy forces are large, what do you purpose us to do?",
    answers: [
      {
        reply: "I will go to the village and ask Eva for help",
        next: "C",
      },
      {
        reply:
          "King, you need to send messengers to the villages asking for more soldiers",
        next: "D",
      },
    ],
  },
  C: {
    question:
      "Hey Eva we are in a trouble will you help us to defeat the enemy?",
    answers: [
      {
        reply: "Yes, I will go right away",
        next: "D",
      },
      {
        reply:
          "Yes, I would like to join you, but I need more time to prepare, to train my dragon",
      },
    ],
  },
  D: {
    question: "What would you like to do now?",
    answers: [
      {
        reply: "Cast Eva's spell to convert the dragon into its full size",
      },
      {
        reply: "Use Ragnar's thunder power",
      },
    ],
  },
};
