import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";

const OptionList = (props) => {
  const { options, handlePressOption } = props;
  //console.log(print);
  return (
    <View>
      {options.map((option, index) => (
        <View key={index}>
          {index !== 0 && (
            <View
              style={{
                borderWidth: 1,
                borderColor: "black",
                marginVertical: 20,
              }}
            />
          )}
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              handlePressOption(option);
            }}
          >
            <Text style={styles.text}>{option.reply}</Text>
          </TouchableOpacity>
        </View>
      ))}
    </View>
  );
};
const styles = StyleSheet.create({
  button: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    borderColor: "#333",
    borderWidth: 2,
    borderRadius: 8,
    alignItems: "center",
  },
  text: {
    color: "#222",
    fontSize: 20,
    fontWeight: "500",
  },
});

export default OptionList;
