import { StyleSheet, Text, View } from "react-native";
import React from "react";

const Scene = () => {
  return (
    // Try setting `flexDirection` to `column`.
    <View style={styles.container}>
      <View style={styles.box1}>
        <Text>Hello</Text>
      </View>
      <View style={styles.box2} />
      <View style={styles.box3} />
    </View>
  );
};

export default Scene;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: "brown",
    borderWidth: 1,
    borderColor: "#336633",
    width: 150,
    height: 150,
  },
  box1: {
    alignItems: "center",
    width: 150,
    height: 50,
    backgroundColor: "powderblue",
  },
  box2: {
    alignItems: "center",
    width: 150,
    height: 50,
    backgroundColor: "skyblue",
  },
  box3: {
    alignItems: "center",
    width: 150,
    height: 50,
    backgroundColor: "steelblue",
  },
});
