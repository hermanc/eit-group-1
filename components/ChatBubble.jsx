import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import OptionList from "./OptionList";

const ChatBubble = (props) => {
  const { text, position, options, handlePressOption } = props;

  let chatBubbleTriangleIndent = -20;
  let chatBubbleTriangleRotate = "200deg";
  let chatBubbleAlignSelf = "flex-start";
  if (position === "right") {
    chatBubbleTriangleRotate = "160deg";
    chatBubbleAlignSelf = "flex-end";
  }

  return (
    <View style={{ flex: -1, alignSelf: chatBubbleAlignSelf }}>
      <View style={styles.chatBubbleUpper}>
        {options ? (
          <OptionList options={options} handlePressOption={handlePressOption} />
        ) : (
          <Text style={styles.text}>{text}</Text>
        )}
      </View>
      <View
        style={[
          styles.chatBubbleTriangle,
          position == "right"
            ? {
                alignSelf: "flex-end",
                left: -25,
                transform: [{ rotate: chatBubbleTriangleRotate }],
              }
            : {
                alignSelf: "flex-start",
                left: 25,
                transform: [{ rotate: chatBubbleTriangleRotate }],
              },
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  chatBubbleUpper: {
    //backgroundColor: "#007aff",
    backgroundColor: "#fff",
    //borderWidth: 2,
    borderColor: "black",
    borderRadius: 5,
    paddingVertical: 20,
    paddingHorizontal: 15,
    marginHorizontal: 10,
    flexWrap: "wrap",
    alignItems: "flex-start",
    width: "50%",
  },
  chatBubbleTriangle: {
    width: 0,
    height: 0,
    right: 10,
    top: -5,
    backgroundColor: "transparent",

    borderLeftWidth: 10,
    borderRightWidth: 10,
    borderBottomWidth: 20,
    //borderBottomColor: "#007aff",
    borderBottomColor: "#fff",
    borderLeftColor: "transparent",
    borderRightColor: "transparent",
  },
  text: {
    color: "#222",
    fontSize: 20,
    fontWeight: "500",
  },
});

export default ChatBubble;
