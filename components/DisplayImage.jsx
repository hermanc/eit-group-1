import React from "react";
import { View, Image, StyleSheet, Dimensions } from "react-native";

const win = Dimensions.get("window");

const DisplayImage = (props) => {
  const { img } = props;
  return (
    <View style={styles.container}>
      <Image style={styles.largeImgMid} source={img} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  largeImgMid: {
    width: 150,
    height: 150,
  },
  largeImgStart: {
    width: 150,
    height: 150,
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
});
export default DisplayImage;
