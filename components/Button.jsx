import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";

const PlayButton = (props) => {
  const { text, handlePress } = props;
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.button} onPress={() => handlePress()}>
        <Text style={styles.text}>{text}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default PlayButton;

const styles = StyleSheet.create({
  container: {
    alignSelf: "center",
    alignItems: "center",
    //backgroundColor: "#f72585",
    backgroundColor: "#fff",
    borderRadius: 50,
    width: 150,
    marginVertical: 20,
    //shadowColor: "#f72585",
    shadowColor: "#fff",
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 4,
      width: 2,
    },
  },
  button: {
    paddingVertical: 10,
    borderRadius: 50,
  },
  text: {
    color: "#222",
    fontSize: 20,
    fontWeight: "600",
  },
});
