import React, { useEffect, useState, useRef } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import ChatBubble from "../ChatBubble";
import DisplayImage from "../DisplayImage";
import Button from "../Button";
import { dialogues } from "../../content";
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  useDerivedValue,
  interpolate,
  Extrapolate,
  withTiming,
  withSequence,
} from "react-native-reanimated";
import { Dimensions } from "react-native";

const deviceWidth = Dimensions.get("window").width;

const MainScreen = () => {
  const [currentLeft, setCurrentLeft] = useState(dialogues["A"]["question"]);
  const [currentRight, setCurrentRight] = useState(dialogues["A"]["answers"]);

  const didMount = useRef(false);

  const chatX = useSharedValue(0);
  const chatOpacity = useDerivedValue(() => {
    return interpolate(
      chatX.value,
      [deviceWidth, 0],
      [0, 1],
      Extrapolate.CLAMP
    );
  });
  const chatAnimatedStyle = useAnimatedStyle(() => {
    return {
      transform: [{ translateX: chatX.value }],
      opacity: chatOpacity.value,
    };
  });

  useEffect(() => {
    if (didMount.current) {
      chatX.value = withSequence(
        withTiming(deviceWidth, { duration: 0 }),
        withTiming(0, { duration: 700 })
      );
    } else {
      didMount.current = true;
    }
  }, [currentLeft]);

  function handlePressOption(option) {
    const nextDialoguePart = option.next;
    const nextDialogue = dialogues[nextDialoguePart];
    if (nextDialogue) {
      setCurrentLeft(nextDialogue["question"]);
      setCurrentRight(nextDialogue["answers"]);
    }
  }

  function handleReset() {
    setCurrentLeft(dialogues["A"]["question"]);
    setCurrentRight(dialogues["A"]["answers"]);
  }
  return (
    <View style={styles.container}>
      <Animated.View style={[chatAnimatedStyle, styles.animationContainer]}>
        <ChatBubble text={currentLeft} position="left" />
        <ChatBubble
          position="right"
          options={currentRight}
          handlePressOption={handlePressOption}
        />

        {/*<DisplayImage img={require("./assets/example_fig.png")} /> */}
      </Animated.View>
      <Button text={"Reset"} handlePress={handleReset} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#070616",
    //alignItems: "center",
    justifyContent: "center",
  },
  animationContainer: {
    flex: 1,
    //borderWidth: 2,
    //borderColor: "#fff",
    justifyContent: "center",
  },
  resetButton: {
    alignSelf: "center",
    alignItems: "center",
    //backgroundColor: "#f72585",
    backgroundColor: "#fff",
    borderRadius: 50,
    width: 150,
    marginVertical: 20,
    //shadowColor: "#f72585",
    shadowColor: "#fff",
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 4,
      width: 2,
    },
  },
});

export default MainScreen;
