import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import Button from "../Button";

const StartScreen = (props) => {
  const { handlePressPlay } = props;
  return (
    <View style={styles.background}>
      <Image
        style={styles.header}
        source={require("../../assets/viking_font.png")}
        //resizeMode="center"
      />
      <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          source={require("../../assets/Ragnar.png")}
          resizeMode="stretch"
        />
        <Image
          style={styles.image}
          source={require("../../assets/Eva.png")}
          resizeMode="stretch"
        />
      </View>
      <Button text={"Play"} handlePress={handlePressPlay} />
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: "#070616",
    justifyContent: "flex-start",
    paddingTop: 40,
  },
  imageContainer: {
    //flexDirection: "row",
    justifyContent: "center",
    alignSelf: "center",
    marginBottom: 10,
  },
  header: {
    alignSelf: "center",
    height: 50,
    width: 300,
    marginVertical: 20,
  },
  image: {
    width: 300,
    height: 270,
    resizeMode: "contain",
    //borderColor: "white",
    //borderWidth: 2,
  },
});

export default StartScreen;
