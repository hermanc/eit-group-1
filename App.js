import React, { useState } from "react";
import { StyleSheet, Text, View, Dimensions } from "react-native";
import MainScreen from "./components/screens/MainScreen";
import StartScreen from "./components/screens/StartScreen";
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  useDerivedValue,
  interpolate,
  Extrapolate,
  withTiming,
  withSequence,
} from "react-native-reanimated";

const deviceWidth = Dimensions.get("window").width;

export default function App() {
  const [started, setStarted] = useState(false);
  const mainScreenX = useSharedValue(deviceWidth);

  function handlePressPlay() {
    setStarted(true);
    mainScreenX.value = withTiming(0);
  }

  const mainScreenAnimatedStyle = useAnimatedStyle(() => {
    return {
      transform: [{ translateX: mainScreenX.value }],
    };
  });
  return (
    <View style={styles.appContainer}>
      {started ? (
        <Animated.View style={[styles.animatedView, mainScreenAnimatedStyle]}>
          <MainScreen />
        </Animated.View>
      ) : (
        <StartScreen handlePressPlay={handlePressPlay} />
      )}
    </View>
  );
}
const styles = StyleSheet.create({
  appContainer: {
    flex: 1,
    backgroundColor: "#070616",
  },
  animatedView: {
    flex: 1,
  },
});
